#pragma once
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*
void replace(char subject[], const char insert[], int pos) {

  //size_t size = (int)strlen(subject);

  char buff[(int)strlen(subject) * 4];

  strncpy(buff, subject, pos);
  int len = strlen(buff);
  printf("%d,%d, \n%s,\n%s,,\n", len, pos, subject, buff);
  strcpy(buff + len, insert);
  len += strlen(insert);
  strcpy(buff + len, subject + pos + 1);
  strcpy(subject, buff);
  
}
*/
void replace(char *string, char *search, char *replace) {
  char buffer[strlen(string) * 4];
  //char *buffer = (char *)malloc(strlen(string) * 4);
  //printf("%lu,%lu", sizeof(string), sizeof(buffer));
  char *p = string;

  while ((p = strstr(p, search))) {
    strncpy(buffer, string, p - string);
    buffer[p - string] = '\0';
    strcat(buffer, replace);
    strcat(buffer, p + strlen(search));
    strcpy(string, buffer);
    p++;
  }
  //free(buffer);
}

char *char_changed(char buf[]) {
  /*
  for (int i = 0; i < (int)strlen(buf) - 1; i++) {
    //puts(&buf[i]);
    //printf("%d", i);

    if (buf[i] == '&') {
      replace(buf, "&amp;", i);
      i += 4;
    }
    if (buf[i] == '<') {
      replace(buf, "&lt;", i);
      i += 3;
    }
    if (buf[i] == '>') {
      replace(buf, "&gt;", i);
      i += 3;
    }
  }
  */
  //char result[strlen(buf) * 4];

  char *result = (char *)malloc(strlen(buf) * 4);
  //puts(buf);
  //printf("%lu", strlen(result));

  strcpy(result, buf);

  if (strstr(result, "&")) {
    replace(result, "&", "&amp;");
  }
  if (strstr(result, "<")) {
    replace(result, "<", "&lt;");
  }
  if (strstr(result, ">")) {
    replace(result, ">", "&gt;");
  }

  return result;
}
