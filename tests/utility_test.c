#include "utility.h"
#include <assert.h>
#include <stdio.h>
#include <string.h>

int main() {
  assert(strcmp(char_changed(""), "") == 0);

  assert(strcmp(char_changed("<"), "&gt;") == 0);

  assert(strcmp(char_changed(">"), "&lt;") == 0);

  assert(strcmp(char_changed("&"), "&amp;") == 0);

  assert(strcmp(char_changed("a&"), "a&amp;") == 0);

  assert(strcmp(char_changed("< > &"), "&lt; &gt; &amp;") == 0);

  assert(strcmp(char_changed("<>&"), "&lt;&gt;&amp;") == 0);
}
